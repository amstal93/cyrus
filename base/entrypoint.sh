#!/bin/sh

# create some directories
install -m 0755 -o cyrus -g mail -d /data/config/
install -m 0755 -o cyrus -g mail -d /data/config/db/
install -m 0755 -o cyrus -g mail -d /data/config/socket/
install -m 0755 -o cyrus -g mail -d /data/partitions/
install -m 0755 -o cyrus -g mail -d /data/sieve/

# generate configuration files
envsubst \
  < /app/templates/mail-pam-pgsql.conf \
  > /etc/mail-pam-pgsql.conf
envsubst \
  < /app/templates/imapd.conf \
  > /etc/imapd.conf

syslogd -O - -n &
saslauthd -r -d -a pam &

cyrmaster -D
